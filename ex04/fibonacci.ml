let fibonacci n = 
	if n < 0
		then -1
	else 
		let rec f_aux n acc acc2= 
		begin 
			if n == 0
				then acc
			else
				(f_aux (n -1) acc2 (acc + acc2))
		end 
	in f_aux n 0 1
let () =
	print_int(fibonacci (-5));
	print_endline(" : should be -1");
	print_int(fibonacci 1);
	print_endline(" : should be 1");
	print_int(fibonacci 3);
	print_endline(" : should be 2");
	print_int(fibonacci 6);
	print_endline(" : should be 8");
	print_int(fibonacci 0);
	print_endline(" : should be 0")

