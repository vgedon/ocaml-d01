let rec converges f x n =
	if n > 0
	then f (x) == x || converges f (f (x)) (n - 1)
	else false



let main () = 
	print_endline (string_of_bool (converges (( * ) 2) 2 5));
	print_endline (string_of_bool (converges (fun x -> x / 2) 2 3));
	print_endline (string_of_bool (converges (fun x -> x / 2) 2 0));
	print_endline (string_of_bool (converges (fun x -> x / 2) 2 5))

let () = main ()