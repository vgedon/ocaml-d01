let leibniz_pi delta = 
	if delta < 0.
	then (-1)
	else begin
		let pi = 4. *. atan 1. in
		let cdelta x =
			let d = (4. *. x) -. pi in
			if d >= 0. then d else (-.d)
		in
		let rec loop i acc =
			let f = float_of_int i in
			let calc = ((-1.) ** f) /. ( 2. *. f +. 1.) +. acc in
			if (cdelta calc) < delta
			then i
			else loop (i + 1) calc 
		in
		loop 0 0.
	end

let main delta = 
	print_string "delta : ";
	print_float (delta);
	print_string " reached in ";
	print_int (leibniz_pi delta);
	print_endline " iteration(s)"

let () = main (-0.01)
let () = main 0.01
let () = main 0.001
let () = main 0.0001
let () = main 0.00001
let () = main 0.000001
let () = main 0.0000001
