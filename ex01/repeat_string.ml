let repeat_string ?(str="x") x = 
	if x < 0
	then "Error"
	else let rec loop x =
		if x = 0
		then ""
		else loop (x - 1) ^ str
	in loop x

let main () =
	print_endline(repeat_string ~str:"coucou" (-1));
	print_endline(repeat_string ~str:"coucou" 0);
	print_endline(repeat_string 5);
	print_endline(repeat_string ~str:"Toto" 5);
	print_endline(repeat_string ~str:"a" 5)

(*******************************************)

let () = main ()