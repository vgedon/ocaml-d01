let repeat_x x = 
	if x < 0
	then "Error"
	else let rec loop x =
		if x = 0
		then ""
		else loop (x - 1) ^ "x"
	in loop x

let main () =
	print_endline(repeat_x (-1));
	print_endline(repeat_x 0);
	print_endline(repeat_x 1);
	print_endline(repeat_x 10)

(*******************************************)

let () = main ()